import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "../../models/Playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output()
  cancel = new EventEmitter();

  @Output()
  save = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  onSave(formRef: NgForm) {
    const draft = {
      ...this.playlist,
      ...formRef.value
    };

    this.save.emit(draft);
  }

  constructor() {}

  ngOnInit() {}
}
