import { Injectable } from "@angular/core";
import { Playlist } from "../../models/Playlist";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ffff00"
    },
    {
      id: 234,
      name: "Angular Top20",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: true,
      color: "#00ffff"
    }
  ]);

  playlistsChange = this.playlists.asObservable();

  getPlaylist(id: Playlist["id"]): Observable<Playlist | null> {
    return this.playlists.pipe(
      map(playlists => playlists.find(p => p.id == id))
    );
  }

  update(draft: Playlist) {
    this.playlists.next(
      this.playlists
        .getValue() //
        .map(p => (p.id == draft.id ? draft : p))
    );

    // const playlists = this.playlists.getValue();
    // const index = playlists.findIndex(p => p.id === draft.id);
    // if (index !== -1) {
    //   this.playlists[index] = draft;
    // }
    // this.playlists.next(playlists);
  }

  constructor() {}
}
