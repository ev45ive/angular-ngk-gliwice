import { Component, OnInit } from "@angular/core";
import { Playlist } from "../../models/Playlist";
import { PlaylistsService } from "../services/playlists.service";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, tap } from "rxjs/operators";

type Modes = "show" | "edit";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  playlistsChange = this.service.playlistsChange;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PlaylistsService
  ) {}

  selectedPlaylistChanges = this.route.paramMap.pipe(
    // tap(paramMap => {
    //   debugger
    // }),
    map(paramMap => paramMap.get("playlist_id")),
    switchMap(id => this.service.getPlaylist(parseInt(id))),
    o => o
  );

  select(playlist: Playlist) {
    this.router.navigate(['/playlists',playlist.id], {
      // relativeTo: this.route
    });
    this.mode = "show";
  }

  mode = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  save(draft: Playlist) {
    this.service.update(draft);
    this.mode = "show";
  }

  ngOnInit() {
    // this.service.playlistsChange.subscribe(playlists => {
    //   this.playlists = playlists;
    // });
  }
}
