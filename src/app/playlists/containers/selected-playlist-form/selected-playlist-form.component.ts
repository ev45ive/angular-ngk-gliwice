import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PlaylistsService } from "../../services/playlists.service";
import { tap, map, switchMap } from "rxjs/operators";
import { Playlist } from "../../../models/Playlist";

@Component({
  selector: "app-selected-playlist-form",
  templateUrl: "./selected-playlist-form.component.html",
  styleUrls: ["./selected-playlist-form.component.scss"]
})
export class SelectedPlaylistFormComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PlaylistsService
  ) {}

  selectedPlaylistChanges = this.route.parent.paramMap.pipe(
    tap(params => {
      // debugger;
    }),
    map(paramMap => paramMap.get("playlist_id")),
    switchMap(id => this.service.getPlaylist(parseInt(id)))
    // o => o
  );

  cancel() {
    this.router.navigate([".."], {
      relativeTo: this.route
    });
  }

  save(draft: Playlist) {
    this.service.update(draft);
    this.router.navigate(["/playlists", draft.id], {
      // relativeTo: this.route
    });
  }

  ngOnInit() {}
}
