import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PlaylistsService } from "../../services/playlists.service";
import { map, switchMap, tap } from "rxjs/operators";
import { Playlist } from '../../../models/Playlist';

@Component({
  selector: "app-selected-playlist",
  templateUrl: "./selected-playlist.component.html",
  styleUrls: ["./selected-playlist.component.scss"]
})
export class SelectedPlaylistComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PlaylistsService
  ) {}

  selectedPlaylistChanges = this.route.paramMap.pipe(
    tap(params => {
      // debugger;
    }),
    map(paramMap => paramMap.get("playlist_id")),
    switchMap(id => this.service.getPlaylist(parseInt(id)))
    // o => o
  );

  edit() {
    this.router.navigate(["./edit"], {
      relativeTo: this.route
    });
  }


  ngOnInit() {}
}
