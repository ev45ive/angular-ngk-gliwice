import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { PlaylistFormComponent } from "./playlist-form/playlist-form.component";
import { ItemsListComponent } from "./items-list/items-list.component";
import { ListItemComponent } from "./list-item/list-item.component";

import { FormsModule } from "@angular/forms";
import { SelectedPlaylistComponent } from './containers/selected-playlist/selected-playlist.component';
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { SelectedPlaylistFormComponent } from './containers/selected-playlist-form/selected-playlist-form.component';

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    ItemsListComponent,
    ListItemComponent,
    SelectedPlaylistComponent,
    SelectedPlaylistFormComponent
  ],
  imports: [CommonModule, FormsModule, 
    PlaylistsRoutingModule],
  exports: [PlaylistsViewComponent]
})
export class PlaylistsModule {}
