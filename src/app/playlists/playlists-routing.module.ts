import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { SelectedPlaylistComponent } from "./containers/selected-playlist/selected-playlist.component";
import { NgModule } from "@angular/core";
import { SelectedPlaylistFormComponent } from "./containers/selected-playlist-form/selected-playlist-form.component";

const routes: Routes = [
  {
    path: "playlists",
    pathMatch: "prefix",
    children: [
      {
        path: "",
        // redirectTo:'/add_new'
        component: PlaylistsViewComponent
      },
      {
        path: ":playlist_id",
        component: PlaylistsViewComponent,
        children: [
          {
            path: "",
            component: SelectedPlaylistComponent
          },
          {
            path: "edit",
            component: SelectedPlaylistFormComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
