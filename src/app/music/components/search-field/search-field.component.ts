import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormArray,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AbstractControl
} from "@angular/forms";
import {
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";

export const censor: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const hasError = ("" + control.value).includes("batman");

  return hasError
    ? {
        censor: { censored: "batman" }
      }
    : null;
};

@Component({
  selector: "app-search-field",
  templateUrl: "./search-field.component.html",
  styleUrls: ["./search-field.component.scss"]
})
export class SearchFieldComponent implements OnInit {
  queryForm = new FormGroup({
    query: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      censor
    ])
  });

  @Input()
  set query(q: string) {
    this.queryForm.get("query").setValue(q, {
      emitEvent: false,
      onlySelf: true
    });

    this.queryForm.get("query").updateValueAndValidity({
      emitEvent: true
    });
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }

  constructor() {
    console.log(this.queryForm);
  }

  ngOnInit() {
    const field = this.queryForm.get("query");

    const searchChanges = field.statusChanges.pipe(
      debounceTime(400),
      filter(status => status == "VALID"),
      withLatestFrom(field.valueChanges, (status, value) => value)
    );

    searchChanges.pipe(filter(query => query.length >= 3)).subscribe(
      // query => console.log(query)
      query => this.search(query)
    );
  }
}
