import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "../../models/Album";

import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";

export const SEARCH_MUSIC_URL = new InjectionToken(
  "Url for music search serve"
);

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  queryChanges = new BehaviorSubject<string>("batman");
  albumsChanges = new BehaviorSubject<Album[]>([]);
  messageChanges = new BehaviorSubject<string>("");

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    @Inject(SEARCH_MUSIC_URL) private search_url: string
  ) {
    console.log(this.albumsChanges)

    this.queryChanges
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        // mergeMap(params =>
        // concatMap(params =>
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.search_url, {
              headers: {
                Authorization: `Bearer ${this.auth.getToken()}`
              },
              params
            })
            .pipe(
              catchError((err, caught) => {
                if (err instanceof HttpErrorResponse) {
                  if (err.status == 401) {
                    this.auth.authorize();
                  }
                  return throwError(err.error.error.message);
                }
                return throwError("Unexpected error");
              })
            )
        ),
        // mergeAll(),
        // concatAll(),
        // switchAll(),
        o => o,
        map(resp => resp.albums.items)
      )
      .subscribe(this.albumsChanges);
    // .subscribe(albums => {
    //   this.albumsChanges.next(albums);
    // });
  }

  search(query: string) {
    this.queryChanges.next(query);
  }

  getAlbums() {
    return this.albumsChanges.asObservable();
  }
}

import {
  pluck,
  map,
  catchError,
  concat,
  startWith,
  mergeMap,
  switchMap,
  switchAll
} from "rxjs/operators";
import {
  from,
  empty,
  of,
  throwError,
  Subject,
  ReplaySubject,
  BehaviorSubject
} from "rxjs";
