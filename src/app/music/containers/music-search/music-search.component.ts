import { Component, OnInit } from "@angular/core";
import { MusicSearchService } from "../../services/music-search.service";
import {
  startWith,
  multicast,
  refCount,
  share,
  publishReplay,
  shareReplay,
  map,
  tap,
  switchMapTo
} from "rxjs/operators";
import { Subject, ConnectableObservable } from "rxjs";
import { Album } from "../../../models/Album";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
})
export class MusicSearchComponent implements OnInit {
  queryChanges = this.searchService.queryChanges;

  albumChanges = this.searchService.getAlbums().pipe(share());

  // this.albumChangesWhenQueryChangs = this.route.queryParamMap.pipe(
  //   map(paramMap => paramMap.get("query")),
  //   tap(query => this.searchService.search(query)),
  //   switchMapTo(this.albumChanges)
  // );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: MusicSearchService
  ) {
    // const query = this.route.snapshot.queryParamMap.get("query");

    this.route.queryParamMap.subscribe(paramMap => {
      const query = paramMap.get("query");
      this.searchService.search(query);
      /* .subscribe(....) */
    });
  }

  search(query: string) {
    this.router.navigate(["/music"], {
      queryParams: {
        query: query
      }
    });
  }

  query = "";

  ngOnInit() {}
}
