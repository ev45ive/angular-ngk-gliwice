import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MusicSearchComponent } from "./containers/music-search/music-search.component";
import { SearchFieldComponent } from "./components/search-field/search-field.component";
import { AlbumsListComponent } from "./components/albums-list/albums-list.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import {
  // MusicSearchService,
  SEARCH_MUSIC_URL
} from "./services/music-search.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFieldComponent,
    AlbumsListComponent,
    AlbumCardComponent
  ],
  imports: [CommonModule, HttpClientModule, ReactiveFormsModule],
  exports: [MusicSearchComponent],
  providers: [
    {
      provide: SEARCH_MUSIC_URL,
      useValue: environment.search_url
    }
    // {
    //   provide: "MUSIC_SEARCH_SERVICE",
    //   useFactory: (url: string) => {
    //     return new MusicSearchService(url);
    //   },
    //   deps: ["SEARCH_MUSIC_URL"]
    // },
    // // {
    //   provide: "MUSIC_SEARCH_SERVICE",
    //   useClass: MusicSearchService
    //   // deps: ["SEARCH_MUSIC_URL"]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicModule {}
