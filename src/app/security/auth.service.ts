import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthConfig {
  auth_url: string;
  client_id: string;
  response_type: "token";
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    const jsonToken = sessionStorage.getItem("token");
    if (jsonToken) {
      this.token = JSON.parse(jsonToken);
    }

    if (location.hash) {
      const p = new HttpParams({
        fromString: location.hash
      });
      const access_token = p.get("#access_token");
      if (access_token) {
        this.token = access_token;
        location.hash = "";
        sessionStorage.setItem("token", JSON.stringify(this.token));
      }
    }
  }

  authorize() {
    const { client_id, response_type, redirect_uri } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri
      }
    });

    const url = `${this.config.auth_url}?${p.toString()}`;
    location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
