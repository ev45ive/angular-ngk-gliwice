import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchComponent } from "./music/containers/music-search/music-search.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "music",
    component: MusicSearchComponent
  },
  {
    path: "**", // Page not found - 404
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      // paramsInheritanceStrategy:'always',
      // useHash: true,

    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
