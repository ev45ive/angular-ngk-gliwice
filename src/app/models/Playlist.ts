import { Track } from './Track';
import { Entity } from './Entity';

export interface Playlist extends Entity {
  favorite: boolean;
  /**
   * HEX color value
   */
  color: string;
  tracks?: Track[];
  // tracks: Array<Track>;
}


