//  https://codepen.io/anon/pen/gNZJaP

export interface Album /* extends Entity */ {
  id: string;
  name: string;
  type: "album";
  images: AlbumImage[];
  artists?: Artist[];
}

export interface Artist {
  id: string;
  name: string;
}

export interface AlbumImage {
  url: string;
  height?: number;
  width?: number;
}

export interface PagingObject<T> {
  items: T[];
  limit?: number;
  total?: number;
  offset?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
